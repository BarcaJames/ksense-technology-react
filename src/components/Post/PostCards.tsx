import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {Post} from "../../types/Post";
import './post-cards.css'

const PostCards = () => {
  let {userId} = useParams();
  const [posts, setPosts] = useState<Post[]>();

  useEffect(() => {
    const fetchData = async () => {
      fetch(`https://jsonplaceholder.typicode.com/users/${userId}/posts`)
          .then(response => response.json())
          .then((data: Post[]) => setPosts(data))
    }
    fetchData();
  }, [userId])

  return (
      <div className='post-container'>
        <div className='header'>List of Post</div>
        <div className='card-container'>
          {posts?.map(({title, body, id}: Post) => <div key={id} className='card'>
            <div className='card-title'>
              {title}
              <hr/>
            </div>
            <div className='card-body'>
              {body}
            </div>
          </div>)}
        </div>
      </div>

  )
}

export default PostCards;