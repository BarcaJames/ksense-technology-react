import {useEffect, useState} from "react";
import {User} from "../../types/User";
import {useNavigate} from "react-router-dom";
import './user-table.css'

const UserTable = () =>{
  const navigate = useNavigate();
  const [users, setUsers] = useState<User[]>();

  useEffect(() => {
    const fetchData = async ()=>{
      fetch("https://jsonplaceholder.typicode.com/users")
          .then(response => response.json())
          .then((data: User[]) => setUsers(data))
    }
    fetchData();
  },[])

  return(
      <div>
        <div className='header'>List of Users</div>
        <div className='table-container'>
          <table>
            <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
            </tr>
            </thead>

            <tbody>
            {users?.map(({id, name, username, email}:User) =>
                <tr key={id} onClick={()=>navigate(`/post/${id}`)}>
                  <td>{id}</td>
                  <td>{name}</td>
                  <td>{username}</td>
                  <td>{email}</td>
                </tr>
            )}
            </tbody>
          </table>
        </div>
      </div>
  )
}

export default UserTable;